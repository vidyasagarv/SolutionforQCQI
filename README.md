# Solution for Quantum Computation and Quantum Information

This is unofficial solution manual for "[Quantum Computation and Quantum Information: 10th Anniversary Edition](http://www.cambridge.org/jp/academic/subjects/physics/quantum-physics-quantum-information-and-quantum-computation/quantum-computation-and-quantum-information-10th-anniversary-edition?format=HB&isbn=9781107002173#BBFv83H3ofgcgG3A.97)" (ISBN-13: 978-1107002173) by Nielsen and Chuang.

The file [SolutionManual.pdf](https://gitlab.com/vidyasagarv/SolutionforQCQI/blob/master/SolutionManual.pdf) contains all the solutions I have updated so far.

There is no guarantee that these solutions are correct. If you have any corrections or suggestions, please add a pull request or contact me.

Also check out [my blog](http://vidyasagarv.com/category/learn-quantum-programming/) where I write about my experiences learning quantum programming.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.


## Other such solutions for this book.

There is no official solution manual for this book so far. But [Goropikari's repository](https://github.com/goropikari/SolutionForQuantumComputationAndQuantumInformation) contains solutions for most problems in Chapter 2.
